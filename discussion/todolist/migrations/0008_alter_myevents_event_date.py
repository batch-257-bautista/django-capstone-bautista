# Generated by Django 4.1.5 on 2023-02-04 06:08

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0007_myevents_event_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='myevents',
            name='event_date',
            field=models.DateField(default=datetime.date.today),
        ),
    ]
